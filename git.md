# Esame version control

## git init

Utilizzo: `git init`

Descrizione: Inizializza un nuovo repository Git nella directory corrente.

## git clone

Utilizzo: `git clone <url>`

Descrizione: Crea una copia locale di un repository remoto. Utile quando si desidera lavorare con un repository esistente.

## git add

Utilizzo: `git add <file>` o `git add .`

Descrizione: Aggiunge un file specifico o tutti i file modificati o nuovi alla staging area in preparazione per il commit. Questo comando è utilizzato per includere le modifiche nel prossimo commit.

## git commit

Utilizzo: `git commit -m "messaggio"`

Descrizione: Effettua un commit dei cambiamenti presenti nella staging area con un messaggio descrittivo. I commit rappresentano una versione stabile del tuo progetto e includono le modifiche effettuate.
Scrivendo solamente git commit si aprirà un editor di testo, grazie al comando -m "messaggio" è possibile scriverlo direttamente sulla riga.

## git status

Utilizzo: `git status`

Descrizione: Mostra lo stato corrente del repository, inclusi i file modificati, nuovi o eliminati. È utile per tenere traccia delle modifiche apportate al tuo progetto.

## git push

Utilizzo: `git push`

Descrizione: Carica le modifiche locali sul repository remoto. Questo comando è usato per condividere le modifiche con gli altri collaboratori o per aggiornare il repository remoto.

## git pull

Utilizzo: `git pull`

Descrizione: Aggiorna la tua copia locale di un repository remoto con le ultime modifiche effettuate da altri collaboratori. Questo comando è utile per sincronizzare il tuo lavoro con gli sviluppi più recenti.

## git branch

Utilizzo: `git branch`

Descrizione: Mostra l'elenco dei rami (branch) presenti nel repository. I rami consentono di lavorare su diverse linee di sviluppo in parallelo e sono utili per organizzare e gestire le modifiche.

## git checkout

Utilizzo: `git checkout <branch>`

Descrizione: Cambia il ramo di lavoro corrente. È utile quando si desidera passare a un ramo diverso per lavorare su una specifica funzionalità o risolvere un problema. 
Per creare un nuovo branch al momento del checkout scriviamo -b alla fine del comando.

## git merge

Utilizzo: `git merge <branch>`

Descrizione: Unisce le modifiche presenti in un ramo specifico con il ramo di lavoro corrente. È utilizzato per combinare i cambiamenti di un ramo con un altro, ad esempio per integrare una funzionalità sviluppata in un ramo di sviluppo principale.

## git log

Utilizzo: `git log`

Descrizione: Mostra la cronologia dei commit effettuati nel repository. Questo comando visualizza informazioni come l'autore del commit, la data e l'ora, e il messaggio associato al commit.
git log --all --decorate --oneline --graph serve per visualizzare un log dei commit 

## git switch

Utilizzo: `git switch <nome_branch>`

Descrizione: Il comando "git switch" viene utilizzato per spostarsi da un branch all'altro nel repository Git. Sostituisce il comando "git checkout" per commutare tra i branch. Quando usi "git switch" seguito dal nome del branch, il tuo repository Git si sposterà sul branch specificato.
Per creare un nuovo branch e spostarsi su di esso si usa -c aggiungendo il nome del nuovo branch.

- Inizializzare un nuovo progetto: `git init`
- Copiare un repository esistente: `git clone <url>`
- Aggiungere file alla staging area: `git add <file>` o `git add .`
- Effettuare un commit delle modifiche: `git commit -m "messaggio"`
- Condividere le modifiche con il repository remoto: `git push`
- Aggiornare la tua copia locale con le ultime modifiche: `git pull`
- Creare e gestire rami (branch) separati: `git branch`, `git checkout`
- Unire i cambiamenti da un ramo all'altro: `git merge`
- Esplorare la cronologia dei commit: `git log`
- Per spostarsi in modo più sicuro da un branch ad un altro: `git switch`
- Creazione di un nuovo repository remoto a quello Git locale: `git remote add origin git@gitlab.com: #nomeutente/nomegruppo.git`